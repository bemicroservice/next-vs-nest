export interface CompanyShort{
  ticker: string;
  name: string
  eps: number;
  peRatio: number;
  marketCap: number;
}

export interface Company{
  ticker: string;
  name: string
  address: string
  description: string
  hasDividend: boolean
  fiscalEnd: string 
  eps: number;
  ebitda: number;
  range52Weeks: number[];
  peRatio: number;
  marketCap: number;
}
import { ApolloClient, gql, InMemoryCache } from "@apollo/client";

/**
 * Initialize apollo graphql client for API communication
 * @returns 
 */
export const apollo = () => new ApolloClient({
  uri: "http://localhost:8001/graphql",
  cache: new InMemoryCache,
})

/**
 * GQL Query for all companies dowload
 * @returns
 */
export const generateAllCompaniesQuery = () => gql`
query{
    queryWholeIndex{
      ticker
      name
      eps
      ebitda
      range52Weeks
      peRatio
      marketCap
    }
}
`
/**
 * GQL Query for single company dowload
 * @returns 
 */
export const generateCompanyDetailsQuery = () => gql`
query CompanyDetails($ticker: String!){
    queryCompanyDetails(ticker: $ticker){
      ticker
      name
      address
      description
      hasDividendDate
      fiscalEnd
      eps
      ebitda
      range52Weeks
      peRatio
      marketCap
    }
}
`
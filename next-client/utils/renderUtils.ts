export const toBillions = (value: number) => `$${Math.round(value / (Math.pow(10, 9)))}B`
export const dollarValue = (value: number) => `$${value}`
export const booleanToWords = (value: boolean) => value ? "Yes" : "No"
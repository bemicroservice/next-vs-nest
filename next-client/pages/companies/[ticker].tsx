import { NextPage, NextPageContext } from "next";
import Head from 'next/head'
import { Company } from "../../graphql/DataModels";
import { apollo, generateCompanyDetailsQuery } from '../../graphql/ApolloClient'
import { booleanToWords, dollarValue, toBillions } from "../../utils/renderUtils";
import styles from '../../components/companyDetails/CompanyDetails.module.sass'
import NumberPackWrap from "../../components/companyDetails/NumberPackWrap";



interface PublicCompanyProps {
  company: Company
}

/**
 * Page for public company details
 * @param props 
 * @returns 
 */
const PublicCompany: NextPage<PublicCompanyProps> = (props) => {
  const { ticker, name, marketCap, eps, ebitda, peRatio, address, description, fiscalEnd, hasDividend, range52Weeks }
    = props.company

  return (
    <div>
      <Head>
        <title>{`${ticker} - Detail Information`}</title>
        <meta name="description" content={description} />
      </Head>

      <main>
        <section className={styles.companyDetails}>
          <div>
            <header>
              <h2>{ticker} ({name}) </h2>
              <p>{description}</p>
            </header>
            <div className={styles.companyNumbers}>
              <NumberPackWrap>
                <tr>
                  <td className={styles.descriptionCell}>Address:</td>
                  <td>{address}</td>
                </tr>
                <tr>
                  <td className={styles.descriptionCell}>52 Weeks:</td>
                  <td>{`From ${dollarValue(range52Weeks[0])} to ${dollarValue(range52Weeks[1])}`}</td>
                </tr>
              </NumberPackWrap>

              <NumberPackWrap>
                <tr>
                  <td className={styles.descriptionCell}>Market Valuation:</td>
                  <td>{toBillions(marketCap)}</td>
                </tr>
                <tr>
                  <td className={styles.descriptionCell}>EPS:</td>
                  <td>{dollarValue(eps)}</td>
                </tr>
                <tr>
                  <td className={styles.descriptionCell}>PE Ratio:</td>
                  <td>{dollarValue(peRatio)}</td>
                </tr>
              </NumberPackWrap>

              <NumberPackWrap>
                <tr>
                  <td className={styles.descriptionCell}>EBITDA:</td>
                  <td>{toBillions(ebitda)}</td>
                </tr>
                <tr>
                  <td className={styles.descriptionCell}>Fiscal End:</td>
                  <td>{fiscalEnd}</td>
                </tr>
                <tr>
                  <td className={styles.descriptionCell}>Dividend Date:</td>
                  <td>{booleanToWords(hasDividend)}</td>
                </tr>
              </NumberPackWrap>
            </div>
          </div>
        </section>
      </main>
    </div>
  )
}

export async function getServerSideProps(context: NextPageContext) {
  const graphqlClient = apollo()
  const { ticker } = context.query

  const { data } = await graphqlClient.query({
    query: generateCompanyDetailsQuery(),
    variables: { ticker }
  });

  return {
    props: {
      company: data.queryCompanyDetails,
    },
  };
}

export default PublicCompany
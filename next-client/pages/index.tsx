import type { NextPage } from 'next'
import Head from 'next/head'
import { apollo, generateAllCompaniesQuery } from '../graphql/ApolloClient'
import { CompanyShort } from '../graphql/DataModels'
import styles from '../components/homepage/Home.module.sass'
import CompanyBox from '../components/homepage/CompanyBox/CompanyBox'
interface HomeProps {
  companies: CompanyShort[]
}

/**
 * Homepage component with list of all index companies
 * @param props 
 * @returns 
 */
const Home: NextPage<HomeProps> = (props) => {

  const { companies } = props
  const renderCompanyBoxes = () => companies.map(company => <CompanyBox {...company} />)

  const aboutFaang = "In finance, “FAANG” is an acronym that refers to the stocks of five prominent American technology companies: Meta (FB) (formerly known as Facebook), Amazon (AMZN), Apple (AAPL), Netflix (NFLX); and Alphabet (GOOG) (formerly known as Google)."

  return (
    <div className={styles.homepage}>
      <Head>
        <title>FAANG Index Data</title>
        <meta name="description" content={aboutFaang} />
      </Head>

      <main>
        <h1>What is FAANG</h1>
        <p>{aboutFaang}</p>
        {renderCompanyBoxes()}
      </main>

    </div>
  )
}

export async function getServerSideProps() {
  const graphqlClient = apollo()

  const { data } = await graphqlClient.query({
    query: generateAllCompaniesQuery(),
  });

  return {
    props: {
      companies: data.queryWholeIndex,
    },
  };
}

export default Home

import styles from './CompanyDetails.module.sass'

/**
 * Wrapper for numeric values of the public tradec company
 */
export default (props: any) =>
  <div className={styles.numberPack}>
    <table>
      {props.children}
    </table>
  </div>
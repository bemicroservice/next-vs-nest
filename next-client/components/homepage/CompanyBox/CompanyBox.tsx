import { CompanyShort } from "../../../graphql/DataModels";
import { toBillions, dollarValue } from "../../../utils/renderUtils";
import styles from './CompanyBox.module.sass'

/**
 * Box with short interview about the company
 */
export default (companyData: CompanyShort) => {
  const {ticker, name, eps, marketCap, peRatio} = companyData

  return(
    <aside className={styles.companyBox}>
      <div>
        <img src={`images/${ticker}.svg`} alt={name} className={styles.companyImage} />
      </div>
      <div>
        <h2>{ticker} ({name}) </h2>
        <dl>
       
           <dt>Market Valuation:</dt>
          <dd>{toBillions(marketCap)}</dd>

          <dt>EPS:</dt>
          <dd>{dollarValue(eps)}</dd>

          <dt>PE Ratio:</dt>
          <dd>{dollarValue(peRatio)}</dd>
        </dl>
      </div>
      <div>
        <a href={`companies/${ticker}`}>More info</a>
      </div>
    </aside>
  )
}
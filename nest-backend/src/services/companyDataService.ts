import { Injectable } from '@nestjs/common';
import { PublicCompany } from '../graphql/generated.definitions';
import { fromJson } from '../utils/dataUtils';

@Injectable()
export class CompanyDataService {

  private INDEX_TICKERS = ["AAPL", "GOOG", "AMZN", "NFLX", "FB"]

  /**
   * Loads data about single FAANG company from database
   * @param ticker Public compaty stocke ticker
   * @returns Public company finance profile
   */
  getCompanyData(ticker: string): PublicCompany {
    try {
      const dbData = fromJson(`./src/fixtures/${ticker}-overview.json`)
      if (!dbData)
        throw new Error("Wrong company ticker for the sample");
  
      return {
        ticker: dbData["Symbol"],
        name: dbData["Name"],
        address: dbData["Address"],
        fiscalEnd: dbData["FiscalYearEnd"],
        hasDividendDate: dbData["DividendDate"] !== "None",
        description: dbData["Description"],
        ebitda: parseFloat(dbData["EBITDA"]),
        eps: parseFloat(dbData["EPS"]),
        range52Weeks: [parseFloat(dbData["52WeekLow"]), parseFloat(dbData["52WeekHigh"])],
        peRatio: parseFloat(dbData["PERatio"]),
        marketCap: parseInt(dbData["MarketCapitalization"])
      }
      
    } catch (error) {
      console.warn(error)
      return undefined
    }
  }

  /**
   * Read and return data for all companies in FAANG index
   * @returns Data for all FAANG companies
   */
  getAllIndexData(): PublicCompany[]{
    return this.INDEX_TICKERS.map(ticker => this.getCompanyData(ticker))
  }
}
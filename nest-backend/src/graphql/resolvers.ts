import { Resolver, Query, Args } from "@nestjs/graphql";
import { CompanyDataService } from "src/services/companyDataService";


@Resolver("PublicCompany")
export class PublicCompanyResolver{

  constructor(private databaseService: CompanyDataService){}

  /**
   * GraphQL resolver for single company data query
   * @param ticker Stock ticker for the public company
   * @returns Data about the company to client
   */
  @Query()
  queryCompanyDetails(@Args("ticker") ticker: string){
    return this.databaseService.getCompanyData(ticker)
  }

/**
   * GraphQL resolver all index companies
   * @returns Data about all FAANG companies to client
 */
  @Query()
  queryWholeIndex(){
    return this.databaseService.getAllIndexData()
  }
}
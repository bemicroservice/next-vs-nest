import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { PublicCompanyResolver } from './graphql/resolvers';
import {CompanyDataService} from './services/companyDataService'

@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      typePaths: ['./**/*.graphql'],
      definitions: {
        path: join(process.cwd(), 'src/graphql/generated.definitions.ts'),
      },
    }),
  ],
  controllers: [],
  providers: [CompanyDataService, PublicCompanyResolver],
})

export class AppModule {}

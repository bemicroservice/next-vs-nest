import {readFileSync} from 'fs'

/**
 * Sync read from JSON file
 * @param path File path
 * @returns JSON data from the file
 */
export const fromJson = (path: string) => JSON.parse(readFileSync(path).toString())

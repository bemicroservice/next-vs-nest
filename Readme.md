# Next.js vs Nest.js
Two technologies with almost identical name. Both run on JavaScript / Typescript, both are used for modern web development. But they are different in purpose. One manages modern SEO-friendly UI, the second provide stable and scalable server application. 

## What is Next.js
Better known member of this sample - React based framework, but with SSR (server-side-render) support. Modern JS libraries provide massive progress in dynamic front-end development, but for the price of SEO issues. SSR solves this issue, because the most of the content is rendered as standalone HTML page. Next. J's offers common SSR, static content generate or SPA mode.

### Next Basics
- Each file in `pages` folder is an independent page component connected with its route.
- Dynamic page names are supported (included in the sample).
- Page is rendered before at the server, the browser display HTML result.
- Includes HTML meta block per single page with dynamic values.

## What is Nest.js
Tired from low-level backend Node.js frameworks? Nest.js works at more advanced level, already includes many connectors for databases, message brokers, has graphql module, healthchecks, loggers, cache management, testing libraries and much more. Everything under one complex backend framework.

### Nest fundamentals
- Angular-like code structure (modules, services, controllers, models, middlewares).
- Strong IoC support - dependency injection support from root module.
- Typescript-default code with strong AoP (Ascpect Oriented Programming) usage.
- Interesting native modules (graphql, websocker, databases, MQ, security, healthchecks, logging, testing....)

## How to combine Next with Nest?
Simple - this sample demostrates one Nest backend service with GraphQL API for a public traded company data and Next frontend client to render company overview. Both apps are independent, implements Apollo for API calls.

## Technologies
- Nest.js Backend (GraphQL Server, data mock)
- Next.js Frontend (SSR, dynamic routes)
- GraphQL - Schema first, Apollo

## Install
1. Install Node.js with NPM
2. Clone the repository
3. Go to Nest app folder, open terminal, and run `npm install`
4. In Nest folder terminal run `npm run start:dev`
5. Go to Next app folder, open terminal, and run `npm install`
6. In Next folder terminal run `npm run dev`
7. Frontend: `localhost:3000`, Graphql playground: `localhost:8001/graphql`